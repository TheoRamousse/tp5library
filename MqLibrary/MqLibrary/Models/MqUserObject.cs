﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MqLibrary.Models
{
    public class MqUserObject
    {
        public string Email { get; set; }
        public string UrlApproveProfile { get; set; }
    }
}
